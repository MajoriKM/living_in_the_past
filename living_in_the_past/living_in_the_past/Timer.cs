﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace living_in_the_past
{
    class Timer
    {

        //---------------------------------
        // Data
        //---------------------------------
        private float timeRemaining = 0f;

        //---------------------------------
        // Behaviour
        //---------------------------------

        public void Update(GameTime gameTime)
        {
          timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            
        }
        //-----------------------------------------
        public void StartTimer()
        {
            
        }
        //----------------------------------------------
        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
        //----------------------------------------------
        public float GetTimeRemaining()
        {
            return timeRemaining;
        }

    }
}
