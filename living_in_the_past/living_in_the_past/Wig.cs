﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace living_in_the_past
{
    class Wig : Tile
    {
        private int scoreValue = 1;


        public Wig(Texture2D newTexture)
            : base(newTexture)
        {
            
        }

        public int GetScore()
        {
            return scoreValue;
        }


    }
}
