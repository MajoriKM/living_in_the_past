﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace living_in_the_past
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {

        int displaywidth;
        int displayheight;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Level level = new Level();


        Dictionary<string, Screen> screens = new Dictionary<string, Screen>();
        Screen currentScreen = null;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Set the game resolution to the current resolution
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - 50;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - 30;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            displaywidth = graphics.GraphicsDevice.Viewport.Width;
            displayheight = graphics.GraphicsDevice.Viewport.Height;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Level level = new Level(this);
            level.LoadContent(Content, GraphicsDevice);

            screens.Add("level", level);

            TitleScreen title = new TitleScreen(this);
            title.LoadContent(Content, GraphicsDevice);
            screens.Add("title", title);

            WinScreen win = new WinScreen(this);
            win.LoadContent(Content, GraphicsDevice);
            screens.Add("win", win);

            GameoverScreen lose = new GameoverScreen(this);
            lose.LoadContent(Content, GraphicsDevice);
            screens.Add("lose", lose);

            currentScreen = title;

            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            currentScreen.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            currentScreen.Draw(spriteBatch);


            base.Draw(gameTime);
        }

        public void ChangeScreen(string screenName)
        {
            if (screens.ContainsKey(screenName))
            {
                // screen does exist
                // set the current screen to it
                currentScreen = screens[screenName];
            }
            
        }

        public int GetDisplayWidth()
        {
            return displaywidth;
        }

        public int GetDisplayHeight()
        {
            return displayheight;
        }
    }
}
