﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace living_in_the_past
{
    class LevelBackground
    {
        private Game1 game;
        private Level level;

        public LevelBackground(Game1 newGame, Level newLevel)
        {
            game = newGame;
            level = newLevel;
        }

        struct graphic2d
        {
            public Texture2D image;         // Texture which holds the image
            public Rectangle rect;          // Rectangle to hold size and position
        }

        private graphic2d lvl1;
        private graphic2d lvl2;
        private graphic2d lvl3;
        private graphic2d lvl4;

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            lvl1.image = content.Load<Texture2D>("Backgrounds/level_back1");
            lvl2.image = content.Load<Texture2D>("Backgrounds/level_back2");
            lvl3.image = content.Load<Texture2D>("Backgrounds/level_back3");
            

            // Work out the ratio for the image depending on screen size
            float ratio = ((float)game.GetDisplayWidth() / lvl1.image.Width);
            lvl1.rect.Width = game.GetDisplayWidth();
            lvl2.rect.Width = game.GetDisplayWidth();
            lvl3.rect.Width = game.GetDisplayWidth();
            

            // Work out new height based on the ratio
            lvl1.rect.Height = (int)(lvl1.image.Height * ratio);
            lvl1.rect.X = 0;
            lvl2.rect.Height = (int)(lvl1.image.Height * ratio);
            lvl2.rect.X = 0;
            lvl3.rect.Height = (int)(lvl1.image.Height * ratio);
            lvl3.rect.X = 0;
           

            // Put image in the middle of the screen on the Y axis
            lvl1.rect.Y = (game.GetDisplayHeight() - lvl1.image.Height) / 70;
            lvl2.rect.Y = (game.GetDisplayHeight() - lvl2.image.Height) / 70;
            lvl3.rect.Y = (game.GetDisplayHeight() - lvl3.image.Height) / 70;
            

            
        }


        public void Draw (SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            if(level.GetCurrentLevel() == 1)
            {
                spriteBatch.Draw(lvl1.image, lvl1.rect, Color.White);
            }
            else if(level.GetCurrentLevel() == 2)
            {
                spriteBatch.Draw(lvl2.image, lvl2.rect, Color.White);
            }
            else if (level.GetCurrentLevel() == 3)
            {
                spriteBatch.Draw(lvl3.image, lvl3.rect, Color.White);
            }

            spriteBatch.End();
        }

    }
}
