﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace living_in_the_past
{
    class Trophy : Tile
    {

        private int scoreValue = 1;

        public Trophy(Texture2D newTexture)
            : base(newTexture)
        {

        }

        public int GetScore()
        {
            return scoreValue;
        }

    }
}
