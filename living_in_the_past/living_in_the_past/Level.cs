﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;

namespace living_in_the_past
{
    class Level : Screen
    {

        // ------------------
        // Data
        // ------------------
        private Game1 game;
        private Tile[,] tiles;
        private Wig wig;
        private Hat hat;
        private Gun gun;
        private Trophy trophy;
        private Knife knife;
        private int currentLevel;
        private LevelBackground lvlBack;
        private Text scoreDisplay;
        private Text timerDisplay;
        private Text abilityDisplay;
        private bool loadNextLevel = false;
        private Timer gameTimer;
        private Ability ability;
        private int score;
        private int time;
        private int abilityTime;
        private Vector2 found = new Vector2(0,-50);

        // Assets
        Texture2D gunSprite;
        Texture2D knifeSprite;
        Texture2D trophySprite;
        Texture2D wigSprite;
        Texture2D hatSprite;
        Texture2D inventorySprite;
        SpriteFont UIFont;
        SoundEffect clickSFX;
        SoundEffect bMusic;
        SoundEffectInstance music;

        // Constant
        private const int LAST_LEVEL = 3;
        private const float LEVEL_TIME = 60f;
        private const float ABILITY_TIME = 5f;
        

        // ------------------
        // Behaviour
        // ------------------


        public Level(Game1 newGame)
        {
            game = newGame;
        }

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {

            gunSprite = content.Load<Texture2D>("Graphics/evidence_gun");
            hatSprite = content.Load<Texture2D>("Graphics/evidence_hat");
            knifeSprite = content.Load<Texture2D>("Graphics/evidence_knife");
            trophySprite = content.Load<Texture2D>("Graphics/evidence_trophy");
            wigSprite = content.Load<Texture2D>("Graphics/evidence_wig");
            inventorySprite = content.Load<Texture2D>("Graphics/inventory");

            clickSFX = content.Load<SoundEffect>("audio/buttonclick");
            bMusic = content.Load<SoundEffect>("audio/background_ambience1");
            music = bMusic.CreateInstance();

            lvlBack = new LevelBackground(game, this);
            lvlBack.LoadContent(content, graphics);

            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            timerDisplay = new Text(UIFont);
            abilityDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));
            timerDisplay.SetPosition(new Vector2(900, 10));
            abilityDisplay.SetPosition(new Vector2(1750, 10));

            gameTimer = new Timer();
            gameTimer.SetTimer(LEVEL_TIME);

            ability = new Ability();
            ability.SetTimer(ABILITY_TIME);

            //TEMP until figured otherwise
            LoadLevel(1);

        }
        //-------------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "LevelFiles/level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");

        }
        //-------------------------
        public void LoadLevel(string fileName)
        {
            

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line " + lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array

            tiles = new Tile[lineWidth, numLines];

            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }
        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Wig
                case 'W':
                    CreateWig(tileX, tileY);
                    break;
                //hat
                case 'H':
                    Createhat(tileX, tileY);
                    break;
                //knife
                case 'K':
                    Createknife(tileX, tileY);
                    break;
                //gun
                case 'G':
                    Creategun(tileX, tileY);
                    break;
                //trophy
                case 'T':
                    Createtrophy(tileX, tileY);
                    break;
                // Blank space
                case '.':
                    
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }
        // ------------------       
        private void CreateWig(int tileX, int tileY)
        {
            wig = new Wig(wigSprite);
            wig.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = wig;
        }
        // ------------------
        private void Createhat(int tileX, int tileY)
        {
            hat = new Hat(hatSprite);
            hat.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = hat;
        }
        // ------------------
        private void Creategun(int tileX, int tileY)
        {
            gun = new Gun(gunSprite);
            gun.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = gun;
        }
        // ------------------
        private void Createtrophy(int tileX, int tileY)
        {
            trophy = new Trophy(trophySprite);
            trophy.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = trophy;
        }
        // ------------------
        private void Createknife(int tileX, int tileY)
        {
            knife = new Knife(knifeSprite);
            knife.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = knife;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            

            time = (int)gameTimer.GetTimeRemaining();

            abilityTime = (int)ability.GetAbilityTimeRemaining();

            //loop background music
            music.IsLooped = true;
            music.Volume = 0.9f;
            if(music.State == SoundState.Stopped)
            {
                music.Play();
            }

            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }
            //Checks the evaluateVictory function 
            //to see if the conditions for beating the level have been met
            EvaluateVictory();

            gameTimer.Update(gameTime);

            ability.Update(gameTime);


            Input(wig,hat,trophy,gun,knife);

            if (loadNextLevel == true)
            {
                if (currentLevel == LAST_LEVEL)
                {
                    LoadLevel(1);
                    game.ChangeScreen("win");
                }
                else
                {
                    LoadLevel(currentLevel + 1);
                    score = 0;
                    gameTimer.SetTimer(LEVEL_TIME);
                    gameTimer.StartTimer();
                }

                loadNextLevel = false;
            }

            if (time <= 0)
            {
                LoadLevel(1);
                game.ChangeScreen("lose");
            }

            if (time <= 0)
            {
                time = 0;
            }

            if (abilityTime <= 0)
            {
                abilityTime = 0;
            }
            else if (abilityTime >= ABILITY_TIME)
            {
                abilityTime = (int)ABILITY_TIME;
            }

            if(ability.GetInUse() != true)
            {
                knife.SetVisible(false);
                gun.SetVisible(false);
            }
            else
            {
                knife.SetVisible(true);
                gun.SetVisible(true);
            }

            //update score
            scoreDisplay.SetTextString("score:  " + score);
            //update Timer
            timerDisplay.SetTextString("Time:  " + time);
            // update ability timer
            abilityDisplay.SetTextString("Ability:  " + abilityTime);
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            lvlBack.Draw(spriteBatch);
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }

            scoreDisplay.Draw(spriteBatch);
            timerDisplay.Draw(spriteBatch);
            abilityDisplay.Draw(spriteBatch);

            
        }
        // ------------------
        public Tile GetTileAtPosition(Vector2 tilePos)
        {
            int posX = (int)tilePos.X;
            int posY = (int)tilePos.Y;
            if (posX >= 0 && posY >= 0 && posX < tiles.GetLength(0) && posY < tiles.GetLength(1))
            {
                return tiles[posX, posY];
            }
            else
            {
                // out of bounds of array
                return null;
            }
        }
        // ------------------
        public void Input(Wig hitWig, Hat hithat, Trophy hitTrophy, Gun hitGun, Knife hitKnife)
        {
            MouseState currentState = Mouse.GetState();

            if (currentState.LeftButton == ButtonState.Pressed
                && wig.GetBounds().Contains(currentState.X, currentState.Y))
            {
                //object found
                clickSFX.Play();

                score += hitWig.GetScore();
                wig.SetPosition(found);
            }
            else if (currentState.LeftButton == ButtonState.Pressed
                && hat.GetBounds().Contains(currentState.X, currentState.Y))
            {
                //object found
                clickSFX.Play();

                score += hithat.GetScore();
                hat.SetPosition(found);
            }
            else if (currentState.LeftButton == ButtonState.Pressed
                && gun.GetBounds().Contains(currentState.X, currentState.Y))
            {
                //object found
                clickSFX.Play();

                score += hitGun.GetScore();
                gun.SetPosition(found);
            }
            else if (currentState.LeftButton == ButtonState.Pressed
                && trophy.GetBounds().Contains(currentState.X, currentState.Y))
            {
                //object found
                clickSFX.Play();

                score += hitTrophy.GetScore();
                trophy.SetPosition(found);
            }
            else if (currentState.LeftButton == ButtonState.Pressed
                && knife.GetBounds().Contains(currentState.X, currentState.Y))
            {
                //object found
                clickSFX.Play();
                score += hitKnife.GetScore();
                knife.SetPosition(found);
            }


        }

        public void EvaluateVictory()
        {
            if(score == 5)
            {
                loadNextLevel = true;
            }
        }

        public int GetCurrentLevel()
        {
            return currentLevel;
        }

    }


}