﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace living_in_the_past
{
    class Knife : Tile
    {

        private int scoreValue = 1;
        public Knife(Texture2D newTexture)
            : base(newTexture)
        {

        }

        public int GetScore()
        {
            return scoreValue;
        }

        public void SetVisible()
        {
            
        }
    }
}
