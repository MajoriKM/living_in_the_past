﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace living_in_the_past
{
    class GameoverScreen :Screen
    {

        //--------------------------
        // Data
        //--------------------------
        private Text gameName;
        private Text startPrompt;
        private Game1 game;

        //--------------------------
        // Behaviour
        //--------------------------

        public GameoverScreen(Game1 endGame)
        {
            game = endGame;
        }

        struct graphic2d
        {
            public Texture2D image;         // Texture which holds the image
            public Rectangle rect;          // Rectangle to hold size and position
        }

        private graphic2d background;

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("Gameover");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("[Press Esc to Quit the Game]");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));

            background.image = content.Load<Texture2D>("backgrounds/gameover");

            // Work out the ratio for the image depending on screen size
            float ratio = ((float)game.GetDisplayWidth() / background.image.Width);
            background.rect.Width = game.GetDisplayWidth();
            // Work out new height based on the ratio
            background.rect.Height = (int)(background.image.Height * ratio);
            background.rect.X = 0;

            // Put image in the middle of the screen on the Y axis
            background.rect.Y = (game.GetDisplayHeight() - background.image.Height) / 100;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(background.image, background.rect, Color.White);
            spriteBatch.End();
            gameName.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {

        }

    }
}
