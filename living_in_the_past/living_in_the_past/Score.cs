﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace living_in_the_past
{
    class Score
    {
        private int score;


        public void addScore(int toAdd)
        {

            // adds provided number to the current score
            score += toAdd;

        }
        //----------------------
        public void ResetScore()
        {
            score = 0;
        }


    }
}
