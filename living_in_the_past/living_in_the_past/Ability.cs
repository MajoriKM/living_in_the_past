﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
namespace living_in_the_past
{
    class Ability
    {

        //---------------------------------
        // Data
        //---------------------------------
        private float abilityTimeRemaining = 0f;
        private bool usable = false;
        private bool inUse = false;

        //---------------------------------
        // Behaviour
        //---------------------------------
       
        public void Update(GameTime gameTime)
        {
            KeyboardState keys = Keyboard.GetState();
            if (usable == true && keys.IsKeyDown(Keys.E))
            {
                abilityTimeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                inUse = true;
            }
            else if (keys.IsKeyUp(Keys.E))
            {
                abilityTimeRemaining += (float)gameTime.ElapsedGameTime.TotalSeconds;
                inUse = false;
            }

            if (abilityTimeRemaining > 0)
            {
                usable = true;
            }
            else
            {
                usable = false;
            }



           
        }
        //----------------------------------------------
        public void SetTimer(float newTime)
        {
            abilityTimeRemaining = newTime;
        }
        //----------------------------------------------
        public bool GetInUse()
        {
            return inUse;
        }
        //----------------------------------------------
        public float GetAbilityTimeRemaining()
        {
            return abilityTimeRemaining;
        }




    }
}
